package com.example.login.Repository;

import com.example.login.Entity.User;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository {

    /**
     * 登录
     * @return
     */
    public String login(int id);

}
