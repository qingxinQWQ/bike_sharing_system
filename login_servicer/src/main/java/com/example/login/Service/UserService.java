package com.example.login.Service;

import com.example.login.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserRepository {
    @Autowired
    @Lazy
//    UserRepository userRepository;
    private UserRepository userRepository;

    @Override
    public String login(int id) {
        String temp=userRepository.login(id);
        System.out.println("查出来力："+temp);
        return temp;
    }
}
