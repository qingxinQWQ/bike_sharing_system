package com.example.login.Controller;//package com.example.servicer.Controller;


import com.example.login.Entity.Response;
import com.example.login.Entity.User;
import com.example.login.Service.UserService;
import com.example.login.Utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

//@Tag(name = "UserControllerApi", description = "管理学生相关接口")
@RestController
//@RequestMapping("/user")
public class UserController {

//    @Lazy
    @Autowired
    private UserService userService;
//
//    @Operation(summary = "登录", description = "登录")
//    @ApiResponse(description = "登录结果", content = @Content(mediaType = "application/json", schema = @Schema(implementation = String.class)))
//    @CrossOrigin(origins ="*" ,maxAge = 3600)
    //登录
    @PostMapping("/login")
    public Response login(@RequestBody User user) {
        System.out.println("进来力  "+user.getPassword()+user.getId());
        String password=userService.login(user.getId());
        Response re=new Response();
        //用户不存在
        if(password==null){
            re.setCode("1");
            re.setMsg("用户名或密码错误");
            return re;
        }
        if(password.equals(user.getPassword())){
//            Map<String,String> info = new HashMap<>();
//            info.put("username",user.getUsername());
            String token = JwtUtil.createJWT(UUID.randomUUID().toString(),user.getUsername(), null);
//            info.put("token",token);
            re.setCode("0");
            re.setData(token);
        }else {
            re.setCode("1");
            re.setMsg("用户名或密码错误");
        }
        return re;
    }

}
