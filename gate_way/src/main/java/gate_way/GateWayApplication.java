package gate_way;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
//@EnableDiscoveryClient(autoRegister = false) // 表示开启服务发现功能，同时设置不自动注册到注册中心
@EnableDiscoveryClient(autoRegister = false) // 表示开启服务发现功能，同时设置不自动注册到注册中心
public class GateWayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class, args);
    }

}
